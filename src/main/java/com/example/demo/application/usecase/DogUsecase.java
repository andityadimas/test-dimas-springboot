package com.example.demo.application.usecase;

import com.example.demo.domain.entities.Dog;

public interface DogUsecase {
    Dog getDogById(Long id);

    Dog createDog(Dog dog);

    Dog updateDog(Long id, Dog updatedDog);

    void deleteDog(Long id);
}
