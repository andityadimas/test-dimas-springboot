package com.example.demo.application.usecase;

import com.example.demo.domain.entities.Dog;

// public class DogUseCaseImpl {

// }

@Service
public class DogUseCaseImpl implements DogUseCase {

    private final DogRepository dogRepository;

    public DogUseCaseImpl(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    @Override
    public Dog getDogById(Long id) {
        return dogRepository.findById(id)
                .orElseThrow(() -> new DogNotFoundException("Dog not found with ID: " + id));
    }

    @Override
    public Dog createDog(Dog dog) {
        // Business logic to create a dog
    }

    @Override
    public Dog updateDog(Long id, Dog updatedDog) {
        // Business logic to update a dog
    }

    @Override
    public void deleteDog(Long id) {
        // Business logic to delete a dog
    }
}