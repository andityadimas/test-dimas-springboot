package com.example.demo.domain.repository;

@Repository
public interface DogRepository extends JpaRepository<Dog, Long> {
}